Implementation of the Nussinov Algorithm for RNA/DNA Base-Pair Prediction in C++ 11

This program uses Dynamical Programming to generate a list of candidate pairing
sequences with the maximum number of canonical base pairs for a RNA Sequence given as input.

It uses very simple scores of 1 for any pairing and an even simpler function to
detect a base pair, which are both excellent points to start extending the program.

# Nussinov-Algorithm

The algorithm takes an input string of RNA Bases (A, C, G, U) and tries
to figure out how to pair the bases in the string (pairs are AU and CG) so that
the number of formed pairs is the highest possible. There are a few rules for pair formation:

- Pairs can be only A with U or C with G
- A base that only form one pair
- The bases in the sequence between the bases that form a pair are called "loop"
- A loop must contain at least one base
- A base can not form between a base inside a loop and another base outside of that loop ("No Pseudoknots Rule")

Ruth Nussinov and George Pieczenik and Jerrold R. Griggs and Daniel J. Kleitman: Algorithms for Loop Matchings.
In: SIAM Journal on Applied Mathematics. Band 35, Nr. 1, Juli 1978, S. 68–82.
https://de.wikipedia.org/wiki/Nussinov-Algorithmus

# Running the Program

Compile with: `g++ -std=c+11 main.cpp -o nussinov`

Run interactively by calling `./nussinov` or pass a RNA sequence directly: `./nussinov "AAUGGGGUAU"`

The output consists of the pairing matrix, a line stating the number of sequence candidates and then a list of all candidates.


```
# ./nussinov "AAAUUU"
```
```
		1	2	3	4	5	6
		A	A	A	U	U	U
1	A	0	0	0	1	2	2
2	A	0	0	0	1	1	2
3	A	0	0	0	0	1	1
4	U	0	0	0	0	0	0
5	U	0	0	0	0	0	0
6	U	0	0	0	0	0	0

Number of Sequence Candidates: 5

123456
AAAUUU
((.)).
((.).)
((..))
(.(.))
.((.))
```