#include <iostream>
#include <string>
#include <vector>

//
//    Implementation of the Nussinov-Algorithm in C++ 11
//
//    Leopold Lindenbauer Copyright (C) 2018, 2019 - MIT License (see LICENSE)
//    lindenbaul86@univie.ac.at
//
//   Compile with: g++ -std=c+11
//


using namespace std;

int match(int i, int j, string &s)
{
	int A {static_cast<int>(s.at(i-1))}, B{static_cast<int>(s.at(j-1))};
	switch (A-B) {
		case 20:
		case -20: return 1;
		break;
		case -4:
		case 4: return 1;
		break;
		default: return 0;
	}
return 1;
}

class node
{
	public:
	vector<node*> branches;
	char Base;

	node();
	node(char);
	node* addchild(node&);
	node* addparent(node&);	

	void print();
};

node::node(): Base {'0'} {}
node::node(char c): Base{c} {}

node* node::addchild(node& leaf) {
	branches.push_back(&leaf);
	return this;
}

node* node::addparent(node& stem) {
	//branch.branches.clear();
	stem.addchild(*this);
	return this;
}

void node::print()
{
	if (branches.size() > 0)
	{
		for (auto& b: branches) 
		{
			b->print();
		}
	} else {
		if (Base != '0')
		{
			cout << Base;
			cout << endl;
		}
	}
	return;
}


class N {
	public:
	vector<vector<N*>> trace;
	int value;
	int i, j;
	int calculate(string &s, vector<vector<N>> &NA);	

	void set(int v);
	N(int i, int j);

	vector<string> addtrackback(string &s);
	int numberofbranches();
};

N::N(int i, int j): value {0}, i{i}, j{j} {}
void N::set(int v) {value = v; return; }

int N::numberofbranches()
{
	int branches = 0;
	if (trace.size() == 0) { return 0; }
	for (auto& t: trace)
	{
		branches++;
		for (auto& b: t)
		{
			branches += b->numberofbranches();
		}
	}
	branches--;
	return branches;
}


vector<string> N::addtrackback(string &s)
{
	vector<string> retval;
	if (trace.size() == 0)
	{
		string container = "";
		for (int loopctr = i; loopctr <= j; loopctr++)
		{
			container = container + '.';
		}
		retval.push_back(container+s);
	}
	for (auto &t: trace)
	{
		if (t.size() == 1)
		{
			if (t.front()->i == i+1)
			{
				string base = ")" + s;
				for (auto m: t.front()->addtrackback(base))
				{
					retval.push_back("(" + m);
				}
			} else {
				string base = "." + s;
				for (auto b: t.front()->addtrackback(base))
				{
					retval.push_back(b);
				}
			}
		} else {
			string base = ")" + s;
			for (auto lb: t.back()->addtrackback(base))
			{
				string rbase = "(" + lb;
				for (auto lb: t.front()->addtrackback(rbase))
				{
					retval.push_back(lb);
				}
			}
		
		}
	}
	return retval;
}

void printtrackbacks(N &Cell, string &s)
{
	cout << endl;
	for (unsigned int i = 1; i <= s.length(); i++)
	{
		cout << (i%10);
	}
	cout << endl;
	for (unsigned int i = 1; i <= s.length(); i++)
	{
		cout << s.at(i-1);
	}
	cout << endl;
	string startseq = "";
	for (auto &seq: Cell.addtrackback(startseq))
	{
		cout << seq << endl;
	}
	return;
}


int N::calculate(string &s, vector<vector<N>> &NA)
{
	int retval {0};
	if ( (i >= j) || (i == (j-1)) ) 
	{
		value = 0;
		return 0;
	} else {
		int candidate {0};
		candidate = NA.at((i)-1).at((j-1)-1).value;
		// not a match. cout << "N:" << candidate << " from i:" << i << " j:" << j << endl;
		trace.push_back(vector<N*> {&NA.at((i)-1).at((j-1)-1)});
		retval = candidate;
		if (match((i), (j), s))
		{
			candidate = NA.at((i+1)-1).at((j-1)-1).value + 1;
			// branchless match between the outermost bases. cout << "M:" << candidate << " from i:" << i+1 << " j:" << j-1 << endl;
			if (candidate >= retval)
			{
				if (candidate > retval)
				{
					trace.clear();
					retval = candidate;
				}
				trace.push_back(vector<N*> {/*&NA.at((i)-1).at((i)-1),*/ &NA.at((i+1)-1).at((j-1)-1)});
			}
		}
		for (int k = (i+1); k < (j-1); k++)
		{
			if (match(k, j, s))
			{
				candidate = NA.at((i)-1).at((k-1)-1).value + NA.at((k+1)-1).at((j-1)-1).value + 1;
				// branching match cout << "B:" << candidate << " from i:" << i+1 << " j:" << j-1 << endl;
				if (candidate >= retval)
				{
					if (candidate > retval)
					{
						trace.clear();
						retval = candidate;
					}
					trace.push_back(vector<N*> {&NA.at((i)-1).at((k-1)-1), &NA.at((k+1)-1).at((j-1)-1)});
				}
			}
		}
	}
	value = retval;
	return retval;
}

void matrixprint(vector<vector<N>> &NA, string &s)
{
	cout << endl;
	cout << '\t' << '\t' << 1;
	for (size_t i = 1; i < s.size(); i++ )
	{
		cout << '\t' << i+1;
	}
	cout << endl;
	cout << '\t' << '\t' << s.at(0);
	for (size_t i = 1; i < s.size(); i++ )
	{
		cout << '\t' << s.at(i);
	}
	cout << endl;
	for (size_t i = 0; i < s.size(); i++ )
	{
		cout << i+1 << '\t' << s.at(i);
		for (size_t j = 0; j < s.size(); j++)
		{
			cout << '\t' << NA.at(i).at(j).value;
		}
		cout << endl;
	}
return;
}
		

void nussinov(string &s)
{
	// create vector field
	vector<vector<N>> NA;
	for (size_t i = 0; i < s.length(); i++)
	{
		vector<N> NRow;
		for (size_t j = 0; j < s.length(); j++)
		{
			NRow.push_back(N(i+1, j+1));
		}
		NA.push_back(NRow);
	}
	
	// fill in from first off-diagonal to the very end
	for (int a = -1; a < static_cast<int>(s.length()); a++)
	{
		for (int b {0}, c {a}; (b < static_cast<int>(s.length()) && c < static_cast<int>(s.length())); b++, c++)
		{
			if (c<0)
			{
				continue;
			} else {
				NA.at(b).at(c).calculate(s, NA);
			}
		}
	}
	// print matrix

	matrixprint(NA, s);
	
	// how many different candidate pairings

	cout << endl << "Number of Sequence Candidates: " << NA.at((1)-1).at(s.length()-1).numberofbranches()+1 << endl;
	
	// trace back

	printtrackbacks(NA.at((1)-1).at(s.length()-1), s);

	//traceback(NA, s, n);	


return;
	
}



int main(int argc, char* argv[]) {
	string s {""};
	if (argc > 1)
	{
		s = argv[1];
	} else {
		cout << "Poldi's Nussinov Calculator - Leopold Lindenbauer 2018\nCall with Sequence as First Parameter or Enter Sequence Below.\nFor Best Experience, Display (or Pipe) the Output in 'less -x4'.\n\nRefer to R.Nussinov, G.Pieczenik, J.R. Griggs, and D.J.Kleitman\nSIAM Journal on Applied Mathematics, 1978(35), 68-82" << endl << endl;
		cout << "Enter Sequence here: ";
		cin >> s;
	}
	nussinov (s);
	return 0;
}
